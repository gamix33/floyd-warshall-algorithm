package agh.dg;

public class Main {

    public static void main(String[] args) {

        String graphFile = "graf.txt";
        int[][] graph = FilesUtils.readGraphFromFile(graphFile);

        WarshallFloyd warshallFloydGraph = new WarshallFloyd(graph);
        //warshallFloydGraph.printDistanceAndPath(17, 6);
        warshallFloydGraph.printDistanceAndPath(1, 20);

    }
}
