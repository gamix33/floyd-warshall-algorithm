package agh.dg;

import java.io.*;

public class FilesUtils {

    private static String readFile(String fileName){
        String fileContent = "";
        int data;

        try (InputStream in = new FileInputStream(fileName)) {
            while ((data = in.read()) != -1) {
                fileContent += ((char) data);
            }
        } catch (IOException e) {
            System.out.println("FILE READ ERROR " + fileName);
        }

        return fileContent;
    }

    public static int[][] readGraphFromFile(String fileName){

        String graphFileContent = readFile(fileName);

        String[] lines = graphFileContent.split("\r\n");
        int size = lines.length;
        int[][] graph = new int[size][size];

        try {
            for (int i = 0; i < size; i++) {
                String line = lines[i];
                line = line.replace(" ", "");
                String[] values = line.split(";");

                if (values.length < 3) {
                    throw new Exception("WEIGHT PARSE ERROR");
                }
                int v1 = Integer.valueOf(values[0]);
                int v2 = Integer.valueOf(values[1]);
                int weight = Integer.valueOf(values[2]);
                graph[v1][v2] = weight;
            }
        }catch (Exception e){
            System.out.println("READ GRAPH DATA ERROR");
        }

        return graph;
    }

}
