package agh.dg;

import java.util.ArrayList;

public class WarshallFloyd {

    private int[][] graph;
    private int[][] p; //poprzedniki
    private int[][] d; //najkrótsze drogi między wierzchołkami

    public WarshallFloyd(int[][] _graph){
        graph = _graph;
        int size = graph.length;

        p = new int[size][size];
        d = new int[size][size];

        for(int v1 = 1; v1 < size; v1++){
            for(int v2 = 1; v2 < size; v2++){
                d[v1][v2] = 999;
                p[v1][v2] = -1;
            }
            d[v1][v1] = 0;
        }

        for(int v1 = 1; v1 < size; v1++){
            for(int v2 = 1; v2 < size; v2++) {
                if(graph[v1][v2] != 0){
                    d[v1][v2] = graph[v1][v2];
                    p[v1][v2] = v1;
                }
            }
        }

        for(int u = 1; u < size; u++) {
            for (int v1 = 1; v1 < size; v1++) {
                for (int v2 = 1; v2 < size; v2++) {
                    if(d[v1][v2] > (d[v1][u] + d[u][v2])){
                        d[v1][v2] = d[v1][u] + d[u][v2];
                        p[v1][v2] = p[u][v2];
                    }
                }
            }
        }
    }

    public void printDistanceAndPath(int v1, int v2) {
        ArrayList<Integer> path = new ArrayList<>();

        System.out.println("\nDistance from " + v1 + " to " + v2 + ": " + d[v1][v2]);
        int u = p[v1][v2];

        path.add(v2);
        path.add(u);

        while (u != v1) {
            u = p[v1][u];
            path.add(u);
        }

        System.out.println("Path from " + v1 + " to " + v2 + ":");
        for (int i = path.size() - 1; i > 0; i--) {
            System.out.print(path.get(i) + " -[" + d[path.get(i)][path.get(i - 1)] + "]-> ");
        }
        System.out.println(path.get(0));
    }
}
